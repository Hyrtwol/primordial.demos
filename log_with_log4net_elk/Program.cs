﻿using System;
using System.Diagnostics;
using log4net;
using log4net.Appender;
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace log_with_log4net
{
    class Program
    {
        static void Main(string[] args)
        {
            ThreadContext.Properties["contextId"] = 12345678L;
            
            ILog logger = LogManager.GetLogger("log_with_log4net");
            IAppender[] appenders = logger.Logger.Repository.GetAppenders();
            foreach (var appender in appenders)
            {
                Debug.WriteLine(appender);
            }
            
            int k = 42;
            int l = 100;
            //logger.Trace("Sample trace message, k={0}, l={1}", k, l);
            logger.DebugFormat("Sample debug message, k={0}, l={1}", k, l);
            logger.InfoFormat("Sample informational message, k={0}, l={1}", k, l);
            logger.WarnFormat("Sample warning message, k={0}, l={1}", k, l);
            logger.ErrorFormat("Sample error message, k={0}, l={1}", k, l);
            logger.FatalFormat("Sample fatal error message, k={0}, l={1}", k, l);
            //logger.Log(LogLevel.Info, "Sample informational message, k={0}, l={1}", k, l);
            try
            {
                throw new NotSupportedException("Not a real exception!");
            }
            catch (Exception ex)
            {
                logger.Error("Log4Net fake error " + DateTime.Now.Ticks, ex);
            }

#if DEBUG
            Console.Write("Done.");
            Console.ReadLine();
#endif
        }
    }
}
