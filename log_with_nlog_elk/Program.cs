﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace log_with_nlog
{
    // https://github.com/nlog/nlog/wiki/Tutorial

    class Program
    {
        static void Main(string[] args)
        {
            var logger = LogManager.GetLogger("log_with_nlog");

            int k = 42;
            int l = 100;
            logger.Trace("Sample trace message, k={0}, l={1}", k, l);
            logger.Debug("Sample debug message, k={0}, l={1}", k, l);
            logger.Info("Sample informational message, k={0}, l={1}", k, l);
            logger.Warn("Sample warning message, k={0}, l={1}", k, l);
            logger.Error("Sample error message, k={0}, l={1}", k, l);
            logger.Fatal("Sample fatal error message, k={0}, l={1}", k, l);
            logger.Log(LogLevel.Info, "Sample informational message, k={0}, l={1}", k, l);

            try
            {
                throw new NotSupportedException("Not a real exception!");
            }
            catch (Exception ex)
            {
                logger.Error(ex, "NLog fake error " + DateTime.Now.Ticks);
            }

#if DEBUG
            Console.Write("Done.");
            Console.ReadLine();
#endif
        }
    }
}
